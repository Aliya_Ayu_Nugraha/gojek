<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');	
	}

	public function index()
	{
		if ($this->session->userdata('login')!=TRUE) {
		redirect('admin/login','refresh');
			}
	
	/*MAPS*/
		$this->load->library('googlemaps');
			
		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 'auto';
		$config['places'] = TRUE;
		$config['placesAutocompleteInputID'] = 'tujuan';
		$this->googlemaps->initialize($config);

		$marker = array();
		$marker['position'] = '37.439, -122.1419';
		$marker['draggable'] = true;
		$marker['ondragend'] = 'updateDatabase(event.latLng.lat(), event.latLng.lng());';
		$this->googlemaps->add_marker($marker);
		$data['map'] = $this->googlemaps->create_map();


		$data['konten']="pesan";
		$data['jumlah']=$this->db->get('driver')->num_rows();
		$data['konten_admin']="konten_admin";

		$where=array('status'=>"menunggu");
		$this->load->model('m_pesanan');
		$data['gojek'] = $this->m_pesanan->get_pesan_driver($where,'pemesanan');
		$data['konten_gojek'] = "pesanan_gojek";
		$this->load->view('template',$data);
	}

	public function pesanan($id_user='')
	{
		$where=array(
			'id_user'=>$this->session->userdata('id_user'),
			'status'=>"menunggu"
		);
		$this->load->model('m_pesanan');
		$data['pesanan'] = $this->m_pesanan->get_pesan($where,'pemesanan')->result();
		$data ['konten'] = "v_pesanan";
		$this->load->view('template', $data);
	}

	public function login()
	{
		if ($this->session->userdata('login')==TRUE) {

			redirect('admin','refresh');
		}
		else{

		$this->load->view('login');
	}
	}

	public function proses_login()
	{
		if($this->input->post('login')){
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_admin');
				if ($this->m_admin->get_login()->num_rows()>0) {
					$data=$this->m_admin->get_login()->row();
					$array=array(
						'login'=> TRUE,
						'id_user' => $data->id_user,
						'nama_user' => $data->nama_user,
						'username'=> $data->username,
						'password'=> $data->password,
						'no_hp'=>$data->no_hp,
						'level' => $data->level
					);

					$this->session->set_userdata( $array );
					redirect('admin','refresh');
				}else if ($this->m_admin->get_login_driver()->num_rows()>0){
					$data=$this->m_admin->get_login_driver()->row();
					$array=array(
						'login'=> TRUE,
						'id_driver' =>$data->id_driver,
						'nama_driver' => $data->nama_driver,
						'username'=> $data->username,
						'password'=> $data->password,
						'level' => $data->level
					);

					$this->session->set_userdata( $array );
					redirect('admin','refresh');
				}else {
				$this->session->set_flashdata('pesan', 'username dan password salah');
				redirect('admin/login','refresh');
				} 
			}else{
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('admin/login','refresh');
			}
		}
	}

	public function register()
	{
		if ($this->input->post('register')) {
			$this->form_validation->set_rules('nama', 'Name', 'trim|required');
			$this->form_validation->set_rules('nomor', 'Nomber Phone', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				if ($this->m_admin->proses_register()) {
					redirect('admin','refresh');
				}else{
					redirect('admin/daftar','refresh');
				}
			} else {
				# code...
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/login','refresh');
	}
	public function profil_admin()
	{
		$data['konten_admin']="profile";
		$this->load->view('template', $data);
	}
	public function profil_gojek($id_driver='')
	{
		$where=array(
			'id_driver'=>$this->session->userdata('id_driver')
		);
		$this->load->model('m_pesanan');
		$data['histori_gojek'] = $this->m_pesanan->get_histori_gojek($where,'transaksi');
		$data ['konten_gojek'] = "profile";
		$this->load->view('template', $data);
	}
	public function profil_customer($id_user='')
	{
		$where=array(
			'id_user'=>$this->session->userdata('id_user')
		);
		$this->load->model('m_pesanan');
		$data['histori_user'] = $this->m_pesanan->get_histori_user($where,'transaksi');
		$data ['konten'] = "profile";
		$this->load->view('template', $data);
	}


	/*tampil dirction*/
	public function direction()
	{
		$this->load->library('googlemaps');
		$data['konten']="direction";

		if ($this->input->post('rute')) {
			$config['directions'] = TRUE;
			$config['directionsDivID']="directionsDiv";
			$config['directionsStart'] = $this->input->post('jemput');
			$config['directionsEnd'] = $this->input->post('tujuan');
			$this->googlemaps->initialize($config);
		}
		$data['map'] = $this->googlemaps->create_map();

		$this->load->view('template', $data);

		if ($this->input->post('pesan')){
			$this->load->model('m_pesanan');
			$this->form_validation->set_rules('jemput', 'jemput', 'trim|required');
			$this->form_validation->set_rules('tujuan', 'tujuan', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
				if ($this->m_pesanan->simpan_pesanan() == TRUE){
				redirect('admin','refresh');
			} else {
				$this->session->set_flashdata('pesan', 'Failed Order');
				redirect('admin','refresh');
			} 
			}else{
				$this->session->set_flashdata('pesan',validation_errors());
				redirect('admin','refresh');
			}
		}
	}

}

/* End of file M_admin.php */
/* Location: ./application/controllers/M_admin.php */