<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_pesanan');
		$this->load->library('googlemaps');
		$this->load->library('pdf');
	}

	public function index()
	{
		$where=array('status'=>"menunggu");
		$this->load->model('m_pesanan');
		$data['gojek'] = $this->m_pesanan->get_pesan_driver($where,'pemesanan');
		$data['konten_gojek'] = "pesanan_gojek";
		$this->load->view('template', $data);	
	}

	public function book($id_pemesanan='')
	{
		$where=array('id_pemesanan'=>$id_pemesanan);

		if ($this->input->post('cek_rute')) {
			$config['directions'] = TRUE;
			$config['directionsStart'] = $this->input->post('jemput');
			$config['directionsEnd'] = $this->input->post('antar');
			$this->googlemaps->initialize($config);	
		}

		$data['transaksi']=$this->m_pesanan->simpan_trans($where,'pemesanan')->result();
		$data['map'] = $this->googlemaps->create_map();
		$this->load->view('book', $data);
	}

	public function edit_status($id)
	{
		$data=$this->m_pesanan->detail($id);
		echo json_encode($data);
	}

	public function terima_pesanan()
	{
		if ($this->input->post('terima')) {
			if ($this->m_pesanan->simpan_transaksi_ok() == TRUE) {
					if ($this->m_pesanan->transaksi_ok() == TRUE) {
						redirect('driver','refresh');
					}
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menerima');
					redirect('driver','refresh');
				}}
		}
	

	public function ganti_status()
	{
		if ($this->input->post('ganti')) {
			if ($this->m_pesanan->transaksi_ok() == TRUE) {
					redirect('driver','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menerima');
					redirect('driver','refresh');
				}}
	}

	public function histori()
	{
		$data['transaksi'] = $this->m_pesanan->get_transaksi();
		$data['konten_admin'] = "histori_pesanan";
		$this->load->view('template', $data);
	}

	public function detailhis($id_transaksi='')
	{	
		if ($this->input->post('lihat')) {
			$config['directions'] = TRUE;
			$config['directionsStart'] = $this->input->post('jemput');
			$config['directionsEnd'] = $this->input->post('antar');
			$this->googlemaps->initialize($config);	
		}
		$data['map'] = $this->googlemaps->create_map();

		$where=array(
			'id_transaksi'=>$id_transaksi
		);
		$data['his'] = $this->m_pesanan->detail_transaksi($where,'transaksi');
		$data['konten_admin']="detailhis";
		$this->load->view('template', $data);
	}

	public function cetak($id_transaksi='')
	{
		$where=array('id_transaksi'=>$id_transaksi);
		$data['nota']= $this->m_pesanan->detail_transaksi($where,'transaksi');
		$this->load->view('cetak_nota', $data);
	}
}

/* End of file Driver.php */
/* Location: ./application/controllers/Driver.php */