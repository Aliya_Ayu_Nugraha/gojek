<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gis extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_gis');
	}

	public function index()
	{
		$tempat = $this->m_gis->getAll();

		$this->load->library('googlemaps');
		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 'auto';

		//halte
		foreach ($tempat as $key ) {
		$lokasi = $key->latitude .','. $key->longitude;
		$namajalan = $key->nama_jalan;
		$marker = array();
		$marker['position'] = $lokasi;
		$marker['infowindow_content'] = $namajalan;
		$marker['icon'] = 'https://png.icons8.com/metro/50/2dc86f/motorcycle.png';
		$this->googlemaps->add_marker($marker);
		}

		$this->googlemaps->initialize($config);
		$data['map'] = $this->googlemaps->create_map();

		
		$data['konten_admin'] = "v_gis";
		$this->load->view('template', $data);
	}

	public function gis_rute()
	{
		$tempat = $this->m_gis->getAll();

		$this->load->library('googlemaps');
		$config['center'] = '37.4419, -122.1419';
		$config['zoom'] = 'auto';

		$polyline = array();
		$polyline['points'] = array('-7.272170, 112.761373',
			    '-7.354090, 112.724443',
			    '-7.352373, 112.729774',
				'-7.296814, 112.738749');
		$this->googlemaps->add_polyline($polyline);

		$this->googlemaps->initialize($config);
		$data['map'] = $this->googlemaps->create_map();
		
		$data['konten_admin'] = "v_gis-rute";
		$this->load->view('template', $data);
	}

	public function addrute()
	{
		if ($this->input->post('simpan')){
			$this->form_validation->set_rules('namajalan', 'name jalan', 'trim|required');
			$this->form_validation->set_rules('latitude', 'latitude', 'trim|required');
			$this->form_validation->set_rules('longitude', 'longitude', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				if ($this->m_gis->simpan() == TRUE){
					redirect('gis','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menyimpan');
					redirect('gis','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('gis','refresh');
			}
		}
	}

}

/* End of file Gis.php */
/* Location: ./application/controllers/Gis.php */