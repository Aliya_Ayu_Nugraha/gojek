<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lupapwd extends CI_Controller {

	public function index()
	{
		$this->load->view('lupa_password');		
	}

	public function kirimpwd()
	{
		$this->load->model('m_lupapwd');
		$link = 'Click on this link - http://localhost/gojek/index.php/lupapwd/gantipwd';
		$tujuan = $this->input->post('email');

		if ($this->input->post('send')) {
			$this->form_validation->set_rules('email', 'email', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				if ($this->m_lupapwd->get_email()->num_rows()>0) {
					$data=$this->m_lupapwd->get_email()->row();
					$email=array(
						'email' => $data->email
					);
					$this->session->set_userdata( $email );
				}

			$this->load->library('email');

			$this->email->from('faridpriya@gmail.com', 'Admin');
			$this->email->to($tujuan);
			
			$this->email->subject('Testing');
			$this->email->message($link);
			
			if($this->email->send()){
				$this->session->set_flashdata('pesan', 'cek email anda!');
				redirect('lupapwd','refresh');
			}else{
				echo $this->email->print_debugger();
			}

			} else {
				$this->session->set_flashdata('pesan', 'gagal');
				redirect('lupapwd','refresh');
			}
		}
	}

	public function gantipwd()
	{
		$this->load->view('gantipwd');
	}

	public function proses_ganti()
	{
		$this->load->model('m_lupapwd');

		$this->form_validation->set_rules('pwdbaru', 'New Password', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			if ($this->m_lupapwd->proses_reset() == TRUE) {
				$this->session->sess_destroy();
				redirect('admin','refresh');
			}else{
				$this->session->set_flashdata('pesan', 'gagal');
				redirect('lupapwd/gantipwd','refresh');
			}
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('lupapwd/gantipwd','refresh');
		}
	}

}

/* End of file Lupapwd.php */
/* Location: ./application/controllers/Lupapwd.php */