<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambahdriver extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_driver');
	}

	public function index()
	{
		$data ['tampil_driver'] = $this->m_driver->get_driver();
		$data['konten_admin'] = "tambah_driver";
		$this->load->view('template',$data);
	}
	public function search()
	{
		$keyword = $this->input->post('keyword');
		$data['tampil_driver']=$this->m_driver->get_driver_keyword($keyword);
		$data['konten_admin']="search";
		$this->load->view('template',$data);
	}

	public function prosestambah()
	{
		if ($this->input->post('adddriver')){
			$this->form_validation->set_rules('nama_driver', 'name driver', 'trim|required');
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			$this->form_validation->set_rules('kendaraan', 'vehicle', 'trim|required');
			$this->form_validation->set_rules('plat', 'plat nomor', 'trim|required');
			$this->form_validation->set_rules('foto', 'foto', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				if ($this->m_driver->simpan_driver() == TRUE){
					redirect('tambahdriver','refresh');
				}else{
					$this->session->set_flashdata('pesan', 'Gagal Menyimpan');
					redirect('tambahdriver','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('tambahdriver','refresh');
			}
		}
	}

	public function hapus($id_driver='')
	{
		if ($this->m_driver->hapus($id_driver)) {
			redirect('tambahdriver','refresh');
		}
	}
	public function detail($id_driver='')
	{
		$where=array(
			'id_driver'=>$id_driver
		);
		$data ['driver'] = $this->m_driver->detil_driver($where,'driver')->result();
		$data ['konten_admin'] = "detail";
		$this->load->view('template', $data);
	}



}

/* End of file Tambahdriver.php */
/* Location: ./application/controllers/Tambahdriver.php */