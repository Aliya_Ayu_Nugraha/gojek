S<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {
	
	public function proses_register()
	{
		$nama = $this->input->post('nama');
		$nomor = $this->input->post('nomor');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');

		$object=array(
			'nama_user'=>$nama,
			'username' =>$username,
			'password' =>$password,
			'no_hp' =>$nomor,
			'level' =>$level
		);
			return $this->db->insert('user',$object);
	}

	public function get_login()
	{
		return $this->db
			->where('username',$this->input->post('username'))
			->where('password',$this->input->post('password'))
			->get('user'); 
	}

	public function get_login_driver()
	{
		return $this->db
			->where('username',$this->input->post('username'))
			->where('password',$this->input->post('password'))
			->get('driver'); 
	}



}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */