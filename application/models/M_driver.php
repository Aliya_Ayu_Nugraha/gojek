<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_driver extends CI_Model {

	public function get_driver()
	{
		$tm_driver =$this->db
		->get('driver')->result();
		return $tm_driver;
	}

	public function get_driver_keyword($keyword)
	{
		$this->db->select('*');
		$this->db->from('driver');
		$this->db->like('nama_driver',$keyword);
		$this->db->or_like('username',$keyword);
		return $this->db->get()->result();
	}
	
	public function simpan_driver()
	{
		$nama_driver = $this->input->post('nama_driver');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$level = $this->input->post('level');
		$kendaraan = $this->input->post('kendaraan');
		$plat = $this->input->post('plat');
		$foto = $this->input->post('foto');

		$object=array(
			'nama_driver'=>$nama_driver,
			'username' =>$username,
			'password' =>$password,
			'level' =>$level,
			'kendaran' =>$kendaraan,
			'plat' =>$plat,
			'foto' =>$foto
		);
			return $this->db->insert('driver',$object);
	}

	public function hapus($id_driver='')
	{
		return $this->db->where('id_driver',$id_driver)->delete('driver');
	}
	public function detil_driver($where, $table)
	{
		return $this->db->get_where($table,$where);
	}

}

/* End of file M_driver.php */
/* Location: ./application/models/M_driver.php */