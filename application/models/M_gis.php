<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_gis extends CI_Model {

	public function simpan()
	{
		$namajalan = $this->input->post('namajalan');
		$lat =$this->input->post('latitude');
		$long = $this->input->post('longitude');

        $object = array(
        	'nama_jalan' => $namajalan,
        	'latitude'	 => $lat,
        	'longitude'  => $long
        );
       	return $this->db->insert('koordinatjalan', $object);
    }

	public function getAll(){
        $query = $this->db->get('koordinatjalan');
        return $query->result();
    }
    public function read($id){
        $this->db->where('id_koordinat', $id);
        $query = $this->db->get('koordinatjalan');
        return $query;
    }	

}

/* End of file M_gis.php */
/* Location: ./application/models/M_gis.php */