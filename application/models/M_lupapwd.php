<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_lupapwd extends CI_Model {

	public function get_email()
	{
		return $this->db
			->where('email',$this->input->post('email'))
			->get('user');
	}	

	public function proses_reset()
	{
		$email=$this->input->post('email');
		$new_password=$this->input->post('pwdbaru');

		$object = array(
			'password'=>$new_password
		);

		$where=array('email'=>$email);

		return $this->db->where($where)->update('user',$object);
	}



}

/* End of file M_lupapwd.php */
/* Location: ./application/models/M_lupapwd.php */