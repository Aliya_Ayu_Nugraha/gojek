<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pesanan extends CI_Model {

	public function get_pesan($where,$table)
	{
		$tm_pesanan=$this->db
					  ->get_where($table,$where);
		return $tm_pesanan;
	}
	public function get_pesan_driver($where,$table)
	{
		$tm_pesanan=$this->db
						->join('user','user.id_user=pemesanan.id_user')
						->get_where($table, $where)
						->result();
		return $tm_pesanan;
	}
	public function get_histori_gojek($where,$table)
	{
		$tm_pesanan=$this->db
					->join('pemesanan','pemesanan.id_pemesanan=transaksi.id_pemesanan')
					->join('user','user.id_user=pemesanan.id_user')
					->get_where($table,$where)->result();
		return $tm_pesanan;
		
	}
	public function get_histori_user($where,$table)
	{
		$tm_pesanan=$this->db
					->join('pemesanan','pemesanan.id_pemesanan=transaksi.id_pemesanan')
					->join('driver','driver.id_driver=transaksi.id_driver')
					->get_where($table,$where)->result();
		return $tm_pesanan;
		
	}

	public function get_transaksi()
	{
		$tm_trans = $this->db->join('pemesanan','pemesanan.id_pemesanan=transaksi.id_pemesanan')
							->join('user','user.id_user=pemesanan.id_user')
							->join('driver','driver.id_driver=transaksi.id_driver')
							->get('transaksi')->result();
		return $tm_trans;
	}

	public function simpan_pesanan()
	{
		$id=$this->session->userdata('id_user');
		$tgl=date('Y-m-d',mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y')));
		$jam= Date("H:i:s");

		$object=array(
			'harga' => '10000',
			'id_user'=>$id,
			'tempat' =>$this->input->post('tujuan'),
			'jemput' =>$this->input->post('jemput'),
			'status' =>$this->input->post('status'),
			'tgl' => $tgl,
			'time' =>$jam
		);
		return $this->db->insert('pemesanan', $object);
	}

	public function simpan_trans($where,$table)
	{
		return $tm_pesanan=$this->db
						->join('user','user.id_user=pemesanan.id_user')
						->get_where($table, $where);
	}

	public function transaksi_ok()
	{
		$id_pemesanan = $this->input->post('id_pemesanan');
		$object2 = array(
			'status' =>$this->input->post('status')
		);
		$where=array('id_pemesanan'=>$id_pemesanan);
		return $this->db->where($where)->update('pemesanan',$object2);

	}

	public function simpan_transaksi_ok()
	{
		$id_pemesanan = $this->input->post('id_pemesanan');
		$id_driver = $this->input->post('id_driver');

		$object = array(
			'id_pemesanan' => $id_pemesanan,
			'id_driver' => $id_driver
		);
		return $this->db->insert('transaksi',$object);
	}

	public function detail($a)
	{
		return $this->db->where('id_pemesanan',$a)
					->get('pemesanan')
					->row();
	}

	public function edit_status()
	{
		$object = array(
			'status' =>$this->input->post('status')
		);
		return $this->db->where('id_pemesanan',$this->input->post('id_pemesanan_lama'))->update('pemesanan',$object);

	}
	public function detail_transaksi($where, $table)
	{
		return $this->db
					->join('driver','driver.id_driver=transaksi.id_driver')
					->join('pemesanan','pemesanan.id_pemesanan=transaksi.id_pemesanan')
					->join('user','user.id_user=pemesanan.id_user')
					->get_where($table,$where)->result();
	}

}

/* End of file M_pesanan.php */
/* Location: ./application/models/M_pesanan.php */