<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?=base_url()?>asset/images/favicon.ico" type="image/ico" />

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=base_url()?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?=base_url()?>asset/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="<?=base_url()?>asset/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?=base_url()?>asset/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?=base_url()?>asset/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=base_url()?>asset/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gojeck</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <a href="<?=base_url('index.php/admin/profil')?>"><img src="<?=base_url()?>asset/images/img.jpg" alt="..." class="img-circle profile_img"></a>

              </div>
              <div class="profile_info">
                <span>Welcome, <?= $this->session->userdata('level');?></span>
                <h2><?= $this->session->userdata('nama_user');?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?=base_url('index.php/admin/logout')?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>


        <!-- page content -->
        <div class="right_col" role="main" style="height: auto;">
          <div class="col-md-3 col-md-offset-4">
  <img src="<?=base_url()?>asset/images/gojek.png" style="width: 90%">
  <h3 align="center"><?= $this->session->userdata('nama_driver')?></h3>
</div>
<div class="col-md-4 col-md-offset-4">
  <?php foreach ($transaksi as $tran) {?>
    <form action="<?=base_url('index.php/driver/terima_pesanan')?>" method="post">
      <input class="form-control"  type="hidden" name ="id_pemesanan" value="<?=$tran->id_pemesanan?>">
      <input class="form-control" type="hidden" name ="id_driver" value="<?= $this->session->userdata('id_driver')?>">
      <div class="form-group">
        <label for="exampleInputEmail1">Customer*</label>
          <h3><?=$tran->nama_user?></h4>
       </div>
       <div class="form-group">
        <label for="exampleInputEmail1">jemput*</label>
          <h3><?=$tran->jemput?></h3>
       </div>
      <div class="form-group">
        <label for="exampleInputEmail1">tujuan*</label>
          <h3><?=$tran->tempat?></h3>
       </div>
       <div class="form-group">
        <label for="exampleInputEmail1">harga*</label>
          <h3><?=$tran->harga?></h3>
       </div>

       <table>
          <div class="radio">
            <label><input type="radio" name="status" value="accept" id="status" checked>selesai</label>
          </div>
        </table>


       <input type="submit" class="btn btn-warning" value="Book" name="terima">
    </form>
    <form action="<?=base_url('index.php/driver/book')?>" method="post">
        <input type="hidden" name="jemput" value="<?=$tran->jemput?>">
        <input type="hidden" name="antar" value="<?=$tran->tempat?>">
        <input type="submit" name="cek_rute" class="btn btn-success" value="Rute">
      </form>
    <!--<form action="<?=base_url('index.php/driver/ganti_status')?>" method="post">
       <div class="form-group">
        <input type="hidden" name="id_pemesanan" value="<?=$tran->id_pemesanan?>">
              
       </div>
        <input type="submit" class="btn btn-danger" value="selesai" name="ganti">
    </form>-->
  <?php } ?>
  </div>
    <?php if ($this->input->post('cek_rute') == TRUE) { ?>
        <?php  
         echo $map['js'];
        echo $map['html'];
        ?>
       <button type="button" class="btn btn-danger" onclick="javascript:history.back()"><span>Kembali</span></button>
       <?php } ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right" >
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?=base_url()?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?=base_url()?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?=base_url()?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?=base_url()?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?=base_url()?>asset/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?=base_url()?>asset/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?=base_url()?>asset/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?=base_url()?>asset/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?=base_url()?>asset/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?=base_url()?>asset/vendors/Flot/jquery.flot.js"></script>
    <script src="<?=base_url()?>asset/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?=base_url()?>asset/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?=base_url()?>asset/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?=base_url()?>asset/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?=base_url()?>asset/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?=base_url()?>asset/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?=base_url()?>asset/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?=base_url()?>asset/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?=base_url()?>asset/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?=base_url()?>asset/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?=base_url()?>asset/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?=base_url()?>asset/vendors/moment/min/moment.min.js"></script>
    <script src="<?=base_url()?>asset/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?=base_url()?>asset/build/js/custom.min.js"></script>

    <script src="<?=base_url()?>asset/js/jquery.min.js"></script>
    <script src="<?=base_url()?>asset/js/config.js"></script>
  
  </body>
</html>
