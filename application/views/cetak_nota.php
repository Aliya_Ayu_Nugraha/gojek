<?php
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */

// Include the main TCPDF library (search for installation path).

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 061');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);


// add a page
$pdf->AddPage();
foreach ($nota as $row) {

$html = '

<style>
	.table{
		width: 70%;
		font-size: 15px;
		margin-left: 50%;
		background-color: #f5f5f5;
		padding: 8px;
	}
</style>

<h1 align="center">PT.Gojek Indonesia</h1>
<table class="table">
	<tr style="width :50%">
		<td>Tanggal</td>
		<td>'.$row->tgl.'</td>
	</tr>
	<tr style="width :50%">
		<td>Time</td>
		<td>'.$row->time.'</td>
	</tr>
	<tr style="width :50%">
		<td>Driver</td>
		<td>'.$row->nama_driver.'</td>
	</tr>
	<tr style="width :50%">
		<td>ID Driver</td>
		<td>'.$row->id_driver.'</td>
	</tr>
	<tr style="width :50%">
		<td>Customer</td>
		<td>'.$row->nama_user.'</td>
	</tr>
	<tr style="width :50%">
		<td>Lokasi Jemput</td>
		<td>'.$row->jemput.'</td>
	</tr>
	<tr style="width :50%">
		<td>Lokasi Antar</td>
		<td>'.$row->tempat.'</td>
	</tr>
	<tr style="width :50%">
		<td>IDR</td>
		<td>'.$row->harga.'</td>
	</tr>
</table>';
}

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Nota', 'I');

//============================================================+
// END OF FILE
//============================================================+
