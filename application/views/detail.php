
<?php foreach ($driver as $det) {?>

<div class="col-md-4">
	<div class="thumbnail">
		<img src="<?=base_url('asset/images/'.$det->foto )?>" style="width:270px">
	</div>
</div>

<div class="col-md-6">
	<table class="table table-hover table-striped">
		<tr>
			<th>Nama Driver</th>
			<td><?=$det->nama_driver;?></td>
		</tr>
		<tr>
			<th>Username</th><td>
			<?=$det->username;?></td>
		</tr>
		<tr>
			<th>Kendaraan</th><td>
			<?=$det->kendaran;?></td>
		</tr>
		<tr>
			<th>Plat</th><td>
			<?=$det->plat;?></td>
		</tr>
	</table>
</div>
<?php } ?>
