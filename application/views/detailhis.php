<div class="row">
<?php foreach ($his as $trans) {?>

<div class="col-md-4 col-md-offset-4" style="background:#F5F5F5">
	<table width="100%">
		<tr>
			<p class="bg-info" style="text-align: center;font-size: 20px;color: black;margin-top: 10px"><?=$trans->tgl?> <?=$trans->time?></p>
		</tr>
	</table>
	<div class="thumbnail" style="height: 80%;width: 80%;margin-top: 10px;margin-left: 37px">
		<img src="<?=base_url('asset/images/'.$trans->foto )?>" style="width: 100%">
	</div>

	<table width="100%" style="opacity: center">
		<tr>
			<th>Nama Driver</th>
			<td><?=$trans->nama_driver?></td>
		</tr>
		<tr>
			<th>Kendaraan</th>
			<td><?=$trans->kendaran?></td>
		</tr>
		<tr>
			<th>Plat</th>
			<td><?=$trans->plat?></td>
		</tr>
	</table>
	<hr style="color: #B0C4DE;width=50%">
	<table style="margin-top: 20px">
		
		<tr>
			<td><img src="<?=base_url()?>asset/images/customer.png" style="width: 20%"></td>
			<td><?=$trans->nama_user?></td>
		</tr>
		<tr>
			<td></td>
			<td><?=$trans->no_hp?></td>
		</tr>
		<tr>
			<td><img src="<?=base_url()?>asset/images/a.png" style="width: 20%"></td>
			<td><?=$trans->jemput?></td>
		</tr>
		<tr>
			<td><img src="<?=base_url()?>asset/images/b.png" style="width: 20%"></td>
			<td style="padding-left: -20px"><?=$trans->tempat?></td>
		</tr>
		<tr>
			<td>
				<form action="<?=base_url('index.php/driver/detailhis')?>" method="post">
					<input type="hidden" name="jemput" value="<?=$trans->jemput?>">
					<input type="hidden" name="antar" value="<?=$trans->tempat?>">
					<input type="submit" name="lihat" class="btn btn-info" value="Lihat Rute"  style="margin-top: 20px">
				</form>
			</td>
			<td>
				<a href="<?=base_url('index.php/driver/cetak/'.$trans->id_transaksi)?>" onclick="edit('<?=$trans->id_transaksi?>')" data-toggle="modal" class="btn btn-success" style="margin-top: 20px">Cetak Nota</a>
			</td>
		</tr>
	</table>
	<tr>
			<p class="bg-info" style="text-align: center;font-size: 20px;color: black;margin-top: 10px">Rp.<?=$trans->harga?></p>
		</tr>
</div>
<?php } ?>


	<?php if ($this->input->post('lihat') == TRUE) { ?>
	<div class="col-md-12">
	<?php echo $map['js'];
        echo $map['html'];
    ?>
    <button type="button" class="btn btn-danger" onclick="javascript:history.back()" style="margin-top: 20px"><span>Kembali</span>
    	</div>
    <?php } ?>

</div>