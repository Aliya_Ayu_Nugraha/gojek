<style type="text/css">
	.adp-directions{
		display: none;
	}
	.adp-summary{
		font-size: 20px;
		text-align: center;
	}
</style>

<h2>Direction</h2>
<div class="col-md-8 col-md-offset-2">
<?php echo $map['js']; ?>
<?php echo $map['html']; ?>
<div id="directionsDiv"></div>
</div>