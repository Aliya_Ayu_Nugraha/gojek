<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=base_url()?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?=base_url()?>asset/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=base_url()?>asset/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
              <h1>Forgot Password</h1>
                <form action="<?=base_url('index.php/lupapwd/kirimpwd')?>" method="post">
                <div>
                  <input name=email type="email" class="form-control" placeholder="Email" required="" />
                </div>

                <div>
                  <input name="send" class="btn btn-default submit" type="submit" value="Send"></input>
                </div>
              </form>
              <?php if($this->session->flashdata('pesan')!=null):?>
                <div class="alert alert-success" style="text-align: center; font-size: 70px;"><?=$this->session->flashdata('pesan');?></div>
              <?php endif ?>

              <div class="clearfix"></div>

              <div class="separator">
                <p>we will send your password to email</p>

                <div class="clearfix"></div>
                <br />
              </div>
            
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form action="<?=base_url('index.php/admin/register')?>" method="post">
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Name" required="" name="nama" />
              </div>
              <div>
                <input type="number" class="form-control" placeholder="Number Phone" required="" name="nomor" />
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="username" style="margin-top: 20px" maxlength="12" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="password" />
              </div>
              <input name="level" type="hidden" value="customer">
              <div>
                <input name="register" class="btn btn-default" type="submit" value="Sign up ">
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
