<?php foreach ($gojek as $pes){ ?>
<div class="col-md-7 col-md-offset-2" style="background: #f3f3f3; margin-top: 20px">
	<table width="100%">
		<tr>
			<td style="padding-top: 20px;"><?=$pes->nama_user?></td>
			<td style="text-align: right;padding-top: 20px;"><?=$pes->tgl?></td>
		</tr>
	</table>
	<hr>
	<table>
		<tr>
			<td><img src="<?=base_url()?>asset/images/a.png" style="width: 50%"></td>
			<td><?=$pes->jemput?></td>
		</tr>
		<tr>
			<td><img src="<?=base_url()?>asset/images/b.png" style="width: 50%"></td>
			<td style="padding-left: -20px"><?=$pes->tempat?></td>
		</tr>
		<tr>
			<td>
				<a href="#<?=$pes->id_pemesanan?>" onclick="edit('<?=$pes->id_pemesanan?>')" data-toggle="modal" class="btn btn-success">Accept</a>
			</td>
		</tr>
	</table>
</div>


<div class="modal fade" id="<?=$pes->id_pemesanan?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Order</h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id_pemesanan_lama" id="id_pemesanan_lama">
		<table style="font-size: 20px;" class="table table-striped">
			<tr>
				<th>Nama</th>
				<td><?=$pes->nama_user?></td>
			</tr>
			<tr>
				<th>Jam</th>
				<td><?=$pes->time?></td>
			</tr>
			<tr>
				<th>Tanggal</th>
				<td><?=$pes->tgl?></td>
			</tr>
			<tr>
				<th>Penjemputan</th>
				<td><?=$pes->jemput?></td>
			</tr>
			<tr>
				<th>Lokasi Antar</th>
				<td><?=$pes->tempat?></td>
			</tr>
			<tr>
				<th>Tarif</th>
				<td><?=$pes->harga?></td>
			</tr>
		</table>

        <a href="<?=base_url('index.php/driver/book/'.$pes->id_pemesanan)?>" onclick="edit('<?=$pes->id_pemesanan?>')" data-toggle="modal" class="btn btn-success">Terima</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php } ?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/driver/edit_status/"+a, 
       dataType:"json",
       success:function(data){
        $("#status").val(data.status);
        $("#id_pemesanan_lama").val(data.id_pemesanan);
      }
      });
    }
</script>
