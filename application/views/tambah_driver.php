
<a href="#tambah" data-toggle="modal" class="btn btn-warning" style="float: right;margin:15px;"><span class="glyphicon glyphicon-plus" style="padding-right: 2px"></span>Add Driver</a>


<div class="col-md-8 col-md-offset-2" style="margin-top: 5%;background-color: #ededed; padding: 20px;border-radius: 10px">
  
<form action="<?=base_url('index.php/tambahdriver/search')?>" method="post">
<input type="text" name="keyword" placeholder="search">
<input type="submit" name="submit_search">
</form>

<table class="table table-hover">
  <tr>
    <th>No.</th>
    <th>Name</th>
    <th>Username</th>
    <th>Action</th>
  </tr>
<?php $no=0;foreach ($tampil_driver as $driver) : $no++;?>
  <tr>
    <td><?=$no?></td>
    <td><?=$driver->nama_driver?></td>
    <td><?=$driver->username?></td>
    <td>
        <a href="<?=base_url('index.php/tambahdriver/hapus/'.$driver->id_driver)?>" onclick="return confirm('Apakah anda yakin ?')" class="btn btn-danger">Hapus</a>
        <a href="<?=base_url('index.php/tambahdriver/detail/'.$driver->id_driver)?>" onclick="edit('<?=$driver->id_driver?>')" data-toggel="modal" class="btn btn-succes">Info</a>


      </td>
  </tr>
  <?php endforeach ?>
</table>
</div>

<!--Poopup tambah-->
<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Add Driver</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="<?=base_url('index.php/tambahdriver/prosestambah')?>" method="post">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input name="nama_driver" type="text" class="form-control" id="inputEmail3" placeholder="Name">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Vehicle</label>
              <div class="col-sm-10">
                <input name="kendaraan" type="text" class="form-control" id="inputEmail3" placeholder="vehicle">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Plat Nomor</label>
              <div class="col-sm-10">
                <input name="plat" type="text" class="form-control" id="inputEmail3" placeholder="Plat Nomor" maxlength="10" onkeyup="this.value = this.value.toUpperCase()">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
              <div class="col-sm-10">
                <input name="username" type="text" class="form-control" id="inputEmail3" placeholder="Username">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
              <div class="col-sm-10">
                <input name="password" type="password" class="form-control" id="inputEmail3" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Foto Profil</label>
              <input type="file" name="foto">
            </div>
                <input name="level" type="hidden" value="gojek">
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <input name="adddriver" type="submit" class="btn btn-success" value="ADD"></input>
              </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->