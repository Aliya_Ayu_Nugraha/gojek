<h3 align="center">Lokasi Halte</h3>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-list-alt"></span> Tambah Rute</h3>
              </div>
              <div class="panel-body">
                  <form action="<?=base_url('index.php/gis/addrute')?>" method="post">
                      <div class="form-group">
                        <label for="namajalan">Nama Jalan</label>
                        <input type="text" class="form-control" name="namajalan">
                      </div>
                      <div class="form-group">
                        <label for="keterangan">latitude</label>
                        <input type="text" class="form-control" name="latitude">
                      </div>
                      <div class="form-group">
                        <label for="keterangan">longitude</label>
                        <input type="text" class="form-control" name="longitude">
                      </div>
                      <div class="form-group">
                        <input type="submit" name="simpan" class="btn btn-primary" value="simpan">
                      </div>
                  </form>
              </div>
            </div>
        </div>
        <div class="col-md-8">
        	<?php echo $map['js']; ?>
			<?php echo $map['html']; ?>
        </div>
    </div>
    <?php if($this->session->flashdata('pesan')!=null):?>
    <div class="alert alert-danger"><?=$this->session->flashdata('pesan');?></div>
<?php endif ?>
</div>
